import React, { Suspense, useEffect, useState } from "react";
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
const Login = React.lazy(() => import("../pages/LoginPage"));
const Main = React.lazy(() => import("../pages/MainPage"));
const QuizCreationPage = React.lazy(() => import("../pages/QuizCreationPage"));
const Quiz = React.lazy(() => import("./Quiz"));
import Popup from "./Popup";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { IntlProvider } from "react-intl";
import { LOCALES } from "../translations/locales";
import { getLanguage } from "./../utils/utils";
import { changeLanguage } from "./../redux/actionCreators";
import { handleIsLogged } from "../redux/actionCreators";
import { useDispatch } from "react-redux";
import { Loader } from "../styles/components";
const App: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [messages, setMessages] = useState({});
    const isLogged = useTypedSelector((state) => state.login.isLogged);
    const locale = useTypedSelector((state) => state.language.locale);

    useEffect(() => {
        getLanguage(locale).then((res) => {
            setMessages(res.default);
        });
    }, [locale]);

    useEffect(() => {
        const localeFromLC = localStorage.getItem("language");
        const selectedLocale = localeFromLC || LOCALES.RUSSIAN.value;
        dispatch(changeLanguage(selectedLocale));
    }, []);

    useEffect(() => {
        const emailFromLC = localStorage.getItem("email");
        const passwordFromLC = localStorage.getItem("password");
        if (emailFromLC && passwordFromLC) {
            dispatch(handleIsLogged());
        }
    }, []);

    useEffect(() => {
        isLogged && navigate("/");
    }, [isLogged]);

    const loader = <Loader tip="Loading" size="large" />;

    return (
        <div>
            <IntlProvider messages={messages} locale={locale} defaultLocale={LOCALES.RUSSIAN.value}>
                <Suspense fallback={loader}>
                    <Routes>
                        <Route path="/" element={<Main />}>
                            <Route path="/" element={<Quiz />} />
                            <Route path="/form/:id" element={<QuizCreationPage />} />
                            <Route path="form" element={<QuizCreationPage />} />
                        </Route>
                        <Route path="/sign-in" element={<Login />} />
                        <Route path="*" element={<Navigate to={isLogged ? "/" : "/sign-in"} />} />
                    </Routes>
                </Suspense>
                <Popup />
            </IntlProvider>
        </div>
    );
};

export default App;
