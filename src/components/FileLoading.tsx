import React from "react";
import { FileLoadingProps, IImageInfo } from "../types/types";
import { Button, HiddenInput, Image, FileName, Flex, IconWrapper, Icon } from "../styles/components";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { useMatch } from "react-router-dom";
import { useTypedSelector } from "../hooks/useTypedSelector";
import iconTrash from "../images/icon-trash.png";
import iconEdit from "../images/icon-edit.png";
import { useDispatch } from "react-redux";
import { editQuiz } from "../redux/actionCreators";

const FileLoading: React.FC<FileLoadingProps> = ({
    handleInputChange,
    currentQuestion,
    imageInfo,
    listItemActive,
    addImage,
}) => {
    const isFormPath = useMatch("/form");
    const isListItemPath = useMatch("/form/:item");
    const dispatch = useDispatch();

    const intl = useIntl();
    const addStr = getText(intl);
    const inputRef = React.useRef<HTMLInputElement>(null);
    const quizzes = useTypedSelector((state) => state.quiz.quizzes);
    const imageNameFromArr = imageInfo[currentQuestion - 1]?.name;

    const dataQuizzes = React.useMemo(() => {
        const arr = quizzes[listItemActive]?.arr;
        const name = quizzes[listItemActive]?.arr[currentQuestion - 1]?.name;
        return { name, arr };
    }, [listItemActive, currentQuestion, quizzes]);

    const handleAddFile = () => {
        inputRef.current?.click();
    };

    const getSrc = (arr: IImageInfo[]): string => {
        const item = arr[currentQuestion - 1].url;
        return typeof item === "string" ? item : "";
    };

    const deleteImageFromQuizzes = () => {
        const newArr = [...dataQuizzes.arr];
        newArr[currentQuestion - 1] = {};
        dispatch(editQuiz({ ...quizzes[listItemActive], arr: newArr }));
    };

    return (
        <>
            <Flex justify="space-between">
                <HiddenInput ref={inputRef} type="file" onChange={handleInputChange} accept=".jpg, .jpeg, .png" />
                <Button type="button" onClick={handleAddFile} color="white">
                    {addStr("question_add_image")}
                </Button>
            </Flex>
            <div style={{ position: "relative" }}>
                {isFormPath && imageNameFromArr && (
                    <>
                        <div style={{ position: "relative" }}>
                            <Image width="100px" minHeight="100px" src={getSrc(imageInfo)} alt="Превью." />
                            <IconWrapper>
                                <Icon onClick={handleAddFile} src={iconEdit} alt="Редактирование." />
                                <Icon
                                    onClick={() => {
                                        addImage({});
                                    }}
                                    src={iconTrash}
                                    alt="Корзина."
                                />
                            </IconWrapper>
                        </div>
                        <FileName>{imageNameFromArr}</FileName>
                    </>
                )}
                {isListItemPath && dataQuizzes.name && (
                    <>
                        <div style={{ position: "relative" }}>
                            <Image width="100px" minHeight="100px" src={getSrc(dataQuizzes.arr)} alt="Превью." />
                            <IconWrapper>
                                <Icon onClick={handleAddFile} src={iconEdit} alt="Редактирование." />
                                <Icon onClick={deleteImageFromQuizzes} src={iconTrash} alt="Корзина." />
                            </IconWrapper>
                        </div>
                        <FileName>{dataQuizzes.name}</FileName>
                    </>
                )}
            </div>
        </>
    );
};
export default FileLoading;
