import React from "react";
import { Col, Button, Form, Input } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useIntl } from "react-intl";
import Question from "./Question";
import QuestionsNavigation from "./QuestionsNavigation";
import { useDispatch } from "react-redux";
import { getText } from "../utils/utils";
import { IQuiz, IFormQuizProps } from "../types/types";
import { addQuiz, editQuiz } from "../redux/actionCreators";
import { BoldText, FormButton } from "../styles/components";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { QUESTION_OBJ, INITIAL_QUIZ_FORM_VALUES } from "../utils/constants";
import { useMatch } from "react-router";

const FormQuiz: React.FC<IFormQuizProps> = ({
    form,
    questions,
    getQuestionsValue,
    currentQuestion,
    changeCurrentQuestion,
    imageInfo,
    addImage,
    clearImageInfo,
    listItemActive,
}) => {
    const intl = useIntl();
    const addStr = getText(intl);
    const dispatch = useDispatch();
    const isListItemPath = useMatch("/form/:item");

    const quizzes = useTypedSelector((state) => state.quiz.quizzes);

    const handleInputChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
        if (evt.target.files) {
            const file = evt.target.files[0];
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onloadend = () => {
                addImage({ url: fileReader.result, name: file.name });
                if (isListItemPath) {
                    const imageArrFromQuizzes = quizzes[listItemActive]?.arr;
                    const newArr = [...imageArrFromQuizzes];
                    newArr[currentQuestion - 1] = { url: fileReader.result, name: file.name };
                    dispatch(editQuiz({ ...quizzes[listItemActive], arr: newArr }));
                }
            };
        }
    };

    React.useEffect(() => {
        getQuestionsValue(form.getFieldValue("questions"));
    }, [currentQuestion]);

    const questionNavigationItems = React.useMemo(
        () =>
            questions?.map((_, i) => {
                const id = String(i + 1);
                return {
                    label: `${addStr("question")} ${id}`,
                    key: id,
                };
            }),
        [questions]
    );

    const onFinish = (values: IQuiz) => {
        const newValues = { ...values, arr: imageInfo };
        const questionFromArray = quizzes.find((item: IQuiz) => {
            return item.quizTitle === values.quizTitle;
        });
        if (questionFromArray) {
            dispatch(editQuiz(newValues));
        } else {
            dispatch(addQuiz(newValues));
        }

        form.resetFields();
        changeCurrentQuestion(1);
        clearImageInfo();
    };

    const onNumberChange = (key: string) => {
        changeCurrentQuestion(Number(key));
    };

    const addNextStep = () => {
        form.setFieldValue("questions", [...form.getFieldValue("questions"), QUESTION_OBJ]);
        changeCurrentQuestion(currentQuestion + 1);
        if (!imageInfo[currentQuestion - 1]) {
            addImage({});
        }
    };

    return (
        <Col offset={1} span={16}>
            <Form
                form={form}
                initialValues={INITIAL_QUIZ_FORM_VALUES}
                layout="vertical"
                name="dynamic_form_item"
                onFinish={onFinish}
            >
                <Form.Item
                    name="quizTitle"
                    label={addStr("form_name_quiz")}
                    rules={[
                        {
                            required: true,
                            message: addStr("form_name_validation"),
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <QuestionsNavigation
                    items={questionNavigationItems}
                    currentQuestion={currentQuestion}
                    onNumberChange={onNumberChange}
                />
                <BoldText>
                    {addStr("form_step")} №{currentQuestion}
                </BoldText>
                <Question
                    currentQuestion={currentQuestion}
                    imageInfo={imageInfo}
                    handleInputChange={handleInputChange}
                    listItemActive={listItemActive}
                    addImage={addImage}
                />
                <Form.Item>
                    <Button onClick={addNextStep} type="primary" htmlType="button" block icon={<PlusOutlined />}>
                        &ensp;
                        {addStr("form_add_step")}
                    </Button>
                </Form.Item>
                <FormButton>
                    <Button type="primary" htmlType="submit">
                        {addStr("form_save")}
                    </Button>
                </FormButton>
            </Form>
        </Col>
    );
};

export default FormQuiz;
