import React from "react";
import { Select } from "antd";
import { useDispatch } from "react-redux";
import { LOCALES } from "../translations/locales";
import { changeLanguage } from "../redux/actionCreators";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { Image, Text } from "../styles/components";

const LanguageSelection: React.FC = () => {
    const locale = useTypedSelector((state) => state.language.locale);
    const dispatch = useDispatch();

    const handleChange = (value: string) => {
        dispatch(changeLanguage(value));
    };

    return (
        <Select
            onChange={handleChange}
            value={locale}
            bordered={false}
            optionLabelProp="label"
            options={Object.values(LOCALES).map((locale) => ({
                value: locale.value,
                label: (
                    <Text>
                        <Image width="20px" src={locale.imagesSrc} alt={locale.value} />
                        &ensp;
                        {locale.label}
                    </Text>
                ),
            }))}
        ></Select>
    );
};

export default LanguageSelection;
