import React from "react";
import { Button } from "antd";
import { LogoutOutlined } from "@ant-design/icons";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useDispatch } from "react-redux";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { changeVisibility, handleIsUnlogged } from "../redux/actionCreators";
import { ModalCenter } from "../styles/components";

const Popup: React.FC = () => {
    const intl = useIntl();
    const addStr = getText(intl);
    const dispatch = useDispatch();
    const isModalOpen = useTypedSelector((state) => state.modal.isModalOpen);

    const signOut = () => {
        dispatch(handleIsUnlogged());
        dispatch(changeVisibility(false));
    };

    const handleCancel = () => {
        dispatch(changeVisibility(false));
    };

    return (
        <ModalCenter title={addStr("popup_exit_question")} open={isModalOpen} onCancel={handleCancel} footer={null}>
            <Button onClick={signOut} size="large" type="primary" icon={<LogoutOutlined />}>
                &ensp;
                {addStr("main_exit")}
            </Button>
        </ModalCenter>
    );
};

export default Popup;
