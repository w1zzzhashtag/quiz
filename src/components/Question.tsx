import React from "react";
import { Col, Button, Form, Input, Select } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { QuestionProps } from "../types/types";
import FileLoading from "./FileLoading";
import { DeleteButton, QuestionInput, RowOptions } from "../styles/components";
import { OPTIONS } from "../utils/constants";

const Question: React.FC<QuestionProps> = ({
    currentQuestion,
    handleInputChange,
    imageInfo,
    listItemActive,
    addImage,
}) => {
    const intl = useIntl();
    const addStr = getText(intl);

    return (
        <>
            <Form.List name="questions">
                {(questionFields) =>
                    questionFields
                        .filter((question) => question.key === currentQuestion - 1)
                        .map((question, index) => {
                            return (
                                <div key={index}>
                                    <Form.Item
                                        {...question}
                                        key={question.key}
                                        name={[currentQuestion - 1, "question"]}
                                        label={addStr("question")}
                                        rules={[
                                            {
                                                required: true,
                                                message: addStr("question_validation"),
                                            },
                                        ]}
                                    >
                                        <Input />
                                    </Form.Item>
                                    <FileLoading
                                        currentQuestion={currentQuestion}
                                        imageInfo={imageInfo}
                                        handleInputChange={handleInputChange}
                                        listItemActive={listItemActive}
                                        addImage={addImage}
                                    />
                                    <RowOptions>
                                        <Col span={6}>{addStr("question_options")}</Col>
                                        <Col offset={8} span={10}>
                                            {addStr("question_type")}: &ensp;
                                            <Form.Item name={[currentQuestion - 1, "type"]}>
                                                <Select
                                                    // defaultValue="Multiple"
                                                    options={[
                                                        {
                                                            value: "Multiple",
                                                            label: addStr("question_type_m"),
                                                        },
                                                        {
                                                            value: "Single",
                                                            label: addStr("question_type_s"),
                                                        },
                                                    ]}
                                                />
                                            </Form.Item>
                                        </Col>
                                    </RowOptions>
                                    <Form.List name={[currentQuestion - 1, "variants"]}>
                                        {(fields, { add, remove }, { errors }) => (
                                            <>
                                                {fields.map((field) => {
                                                    return (
                                                        <Form.Item {...OPTIONS} required={false} key={field.key}>
                                                            <Form.Item
                                                                {...field}
                                                                validateTrigger={["onChange", "onBlur"]}
                                                                rules={[
                                                                    {
                                                                        required: true,
                                                                        whitespace: true,
                                                                        message: addStr("question_options_validation"),
                                                                    },
                                                                ]}
                                                                noStyle
                                                            >
                                                                <QuestionInput
                                                                    placeholder={addStr("question_options_placeholder")}
                                                                />
                                                            </Form.Item>
                                                            {fields.length > 1 ? (
                                                                <DeleteButton onClick={() => remove(field.name)} />
                                                            ) : null}
                                                        </Form.Item>
                                                    );
                                                })}
                                                <Form.Item>
                                                    <Button onClick={() => add()} icon={<PlusOutlined />}>
                                                        &ensp;
                                                        {addStr("question_add_option")}
                                                    </Button>
                                                    <Form.ErrorList errors={errors} />
                                                </Form.Item>
                                            </>
                                        )}
                                    </Form.List>
                                </div>
                            );
                        })
                }
            </Form.List>
        </>
    );
};

export default Question;
