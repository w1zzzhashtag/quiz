import React from "react";
import { Tabs } from "antd";
import { QuestionsNavigationProps } from "../types/types";

const QuestionsNavigation: React.FC<QuestionsNavigationProps> = ({ onNumberChange, items, currentQuestion }) => {
    return <Tabs activeKey={String(currentQuestion)} onChange={onNumberChange} type="card" items={items} />;
};

export default QuestionsNavigation;
