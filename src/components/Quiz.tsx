import React from "react";
import { Link } from "react-router-dom";
import { Button, Tooltip } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { Flex } from "../styles/components";

const Quiz: React.FC = () => {
    const intl = useIntl();
    const addStr = getText(intl);

    return (
        <Flex justify="center" padding="100px">
            <Tooltip title="Добавить новый quiz">
                <Link to="/form">
                    <Button type="primary" size="large" icon={<PlusOutlined />}>
                        {addStr("quiz_create")}
                    </Button>
                </Link>
            </Tooltip>
        </Flex>
    );
};

export default Quiz;
