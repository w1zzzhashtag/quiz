import React from "react";
import { Col, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useIntl } from "react-intl";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { getText } from "../utils/utils";
import { FormList, ListItem, ListItemText } from "../styles/components";
import { IQuizzesListProps } from "../types/types";
import { Link, useMatch, useNavigate } from "react-router-dom";
import { LIST_WITH_EMPTY_QUIZ } from "../utils/constants";

const QuizzesList: React.FC<IQuizzesListProps> = ({
    form,
    getQuestionsValue,
    changeCurrentQuestion,
    changeListItemActive,
    listItemActive,
}) => {
    const quizzes = useTypedSelector((state) => state.quiz.quizzes);
    const navigate = useNavigate();
    const intl = useIntl();
    const addStr = getText(intl);
    const isFormPath = useMatch("/form");
    const isListItemPath = useMatch("/form/:item");
    const listOfQuizzes = React.useMemo(() => {
        return quizzes.length === 0 ? LIST_WITH_EMPTY_QUIZ : quizzes;
    }, [quizzes]);

    const handleQuizClick = (index: number) => {
        form.setFieldValue("quizTitle", quizzes[index].quizTitle);
        form.setFieldValue("questions", quizzes[index].questions);
        getQuestionsValue(form.getFieldValue("questions"));
        changeListItemActive(index);
    };

    const handleButtonAddClick = () => {
        form.resetFields();
        navigate("/form");
        changeCurrentQuestion(1);
        changeListItemActive(0);
    };

    return (
        <Col span={7}>
            <FormList
                margin={`${isFormPath ? "0px" : "30px"}`}
                size="small"
                dataSource={listOfQuizzes}
                renderItem={(item, index) => (
                    <Link to={`/form/${index}`}>
                        <ListItem
                            onClick={() => {
                                handleQuizClick(index);
                            }}
                            $active={isListItemPath && listItemActive === index}
                        >
                            {quizzes[index]?.quizTitle}
                        </ListItem>
                    </Link>
                )}
            />
            {isFormPath && <ListItemText>{addStr("new_quiz")}</ListItemText>}
            <Button onClick={handleButtonAddClick} block size="middle" type="primary" icon={<PlusOutlined />}>
                &ensp;
                {addStr("form_add_quiz")}
            </Button>
        </Col>
    );
};

export default QuizzesList;
