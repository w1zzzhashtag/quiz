import React from "react";
import { Navigate } from "react-router-dom";
import { useTypedSelector } from "./../hooks/useTypedSelector";

const AuthRedirect = <P extends object>(Component: React.ComponentType<P>) => {
    return Object.assign(
        (props: P) => {
            const isLogged = useTypedSelector((state) => state.login.isLogged);
            if (!isLogged) return <Navigate to="/sign-in" />;
            return <Component {...props} />;
        },
        {
            displayName: "AuthRedirect",
        }
    );
};

export default AuthRedirect;
