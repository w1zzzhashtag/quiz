import React from "react";
import { Button, Checkbox, Form, Input, Row, Col } from "antd";
import LanguageSelection from "../components/LanguageSelection";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { notification } from "antd";
import { handleIsLogged } from "../redux/actionCreators";
import { CheckboxChangeEvent } from "antd/es/checkbox";
import { ILoginData } from "../types/types";
import { FormWrapper, TitleCenter } from "../styles/components";
import { EMAIL_FOR_LOGIN, PASSWORD_FOR_LOGIN } from "../utils/constants";

const Login: React.FC = () => {
    const [form] = Form.useForm();
    const intl = useIntl();
    const addStr = getText(intl);
    const history = useNavigate();
    const dispatch = useDispatch();
    const [valueCheckbox, setValueCheckbox] = React.useState<boolean>(true);

    const email = localStorage.getItem("email") || form.getFieldValue("email");
    const password = localStorage.getItem("password") || form.getFieldValue("password");

    form.setFieldsValue({
        email: email,
        password: password,
    });

    const onChange = (e: CheckboxChangeEvent) => {
        setValueCheckbox(e.target.checked);
        if (e.target.checked === false) {
            localStorage.removeItem("email");
            localStorage.removeItem("password");
        }
    };

    const onFinish = (values: ILoginData) => {
        if (values.email == EMAIL_FOR_LOGIN && values.password == PASSWORD_FOR_LOGIN) {
            if (valueCheckbox) {
                localStorage.setItem("email", values.email);
                localStorage.setItem("password", values.password);
            }
            dispatch(handleIsLogged());
            history("/");
            return;
        }
        notification.error({
            message: addStr("login_error"),
            description: addStr("login_description"),
            placement: "top",
            duration: 2,
        });
    };

    return (
        <FormWrapper>
            <Form form={form} name="basic" onFinish={onFinish} autoComplete="off">
                <TitleCenter level={5}>{addStr("login_title")}</TitleCenter>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: addStr("login_validation"),
                        },
                    ]}
                >
                    <Input placeholder={addStr("login_placeholder")} />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: addStr("login_password_validation"),
                        },
                    ]}
                >
                    <Input.Password placeholder={addStr("login_password_placeholder")} />
                </Form.Item>
                <Form.Item name="remember" valuePropName="checked">
                    <Row>
                        <Col span={8}>
                            <Checkbox defaultChecked={valueCheckbox} onChange={onChange}>
                                {addStr("login_remember")}
                            </Checkbox>
                        </Col>
                        <Col span={7} offset={9}>
                            <LanguageSelection />
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item>
                    <Button block type="primary" htmlType="submit">
                        {addStr("login_button")}
                    </Button>
                </Form.Item>
            </Form>
        </FormWrapper>
    );
};

export default Login;
