import React from "react";
import { Outlet } from "react-router-dom";
import { Layout, Divider, Col } from "antd";
import LanguageSelection from "../components/LanguageSelection";
import { useDispatch } from "react-redux";
import { useIntl } from "react-intl";
import { getText } from "../utils/utils";
import { changeVisibility } from "../redux/actionCreators";
import AuthRedirect from "../hoc/AuthRedirect";
import { NewLayout, RowHeader, NewLink, NewCol, NewContent } from "../styles/components";

const { Header, Footer } = Layout;

const Main: React.FC = () => {
    const intl = useIntl();
    const addStr = getText(intl);
    const dispatch = useDispatch();

    const showModal = () => {
        dispatch(changeVisibility(true));
    };

    return (
        <NewLayout>
            <Header>
                <RowHeader wrap={false}>
                    <Col span={5}>
                        <NewLink to="/">{addStr("main_quiz")}</NewLink>
                    </Col>
                    <NewCol onClick={showModal} span={5} offset={8}>
                        {addStr("main_exit")}
                    </NewCol>
                    <NewCol span={5}>
                        <LanguageSelection />
                    </NewCol>
                </RowHeader>
            </Header>
            <NewContent>
                <div>
                    <Outlet />
                </div>
            </NewContent>
            <Divider />
            <Footer>©2022</Footer>
        </NewLayout>
    );
};

export default AuthRedirect(Main);
