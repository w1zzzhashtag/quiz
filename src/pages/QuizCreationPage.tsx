import { Row } from "antd";
import React from "react";
import FormQuiz from "../components/FormQuiz";
import QuizzesList from "../components/QuizzesList";
import { IQuiz, IQuestion, IImageInfo } from "../types/types";
import { Form } from "antd";

const QuizCreationPage = () => {
    const [form] = Form.useForm<IQuiz>();
    const [questions, setQuestions] = React.useState<IQuestion[]>([]);
    const [currentQuestion, setCurrentQuestion] = React.useState<number>(1);
    const [imageInfo, setImageInfo] = React.useState<IImageInfo[]>([]);
    const [listItemActive, setListItemActive] = React.useState<number>(0);

    const getQuestionsValue = (questions: IQuestion[]) => {
        setQuestions(questions);
    };

    const changeCurrentQuestion = (currentQuestion: number) => {
        setCurrentQuestion(currentQuestion);
    };

    const addImage = (imageInfoObj: IImageInfo) => {
        const arr = [...imageInfo];
        arr[currentQuestion - 1] = imageInfoObj;
        setImageInfo(arr);
    };

    const clearImageInfo = () => {
        setImageInfo([]);
    };

    const changeListItemActive = (index: number) => {
        setListItemActive(index);
    };

    return (
        <Row>
            <QuizzesList
                form={form}
                getQuestionsValue={getQuestionsValue}
                changeCurrentQuestion={changeCurrentQuestion}
                changeListItemActive={changeListItemActive}
                listItemActive={listItemActive}
            />
            <FormQuiz
                form={form}
                questions={questions}
                getQuestionsValue={getQuestionsValue}
                currentQuestion={currentQuestion}
                changeCurrentQuestion={changeCurrentQuestion}
                imageInfo={imageInfo}
                addImage={addImage}
                clearImageInfo={clearImageInfo}
                listItemActive={listItemActive}
            />
        </Row>
    );
};

export default QuizCreationPage;
