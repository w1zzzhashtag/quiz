import { IQuiz } from "../types/types";
import { authActionTypes } from "../types/types";
import { CHANGE_LANGUAGE, POPUP_VISIBILITY, ADD_QUIZ, CHANGE_QUIZZES } from "../utils/constants";

export function changeLanguage(value: string) {
    localStorage.setItem("language", value);
    return {
        type: CHANGE_LANGUAGE,
        payload: value,
    };
}

export function changeVisibility(value: boolean) {
    return {
        type: POPUP_VISIBILITY,
        payload: value,
    };
}

export function handleIsLogged() {
    return {
        type: authActionTypes.IS_LOGGED,
    };
}

export function handleIsUnlogged() {
    return {
        type: authActionTypes.IS_UNLOGGED,
    };
}

export function addQuiz(value: IQuiz) {
    return {
        type: ADD_QUIZ,
        payload: value,
    };
}

export function editQuiz(value: IQuiz) {
    return {
        type: CHANGE_QUIZZES,
        payload: value,
    };
}
