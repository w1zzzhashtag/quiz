import { createStore, combineReducers } from "redux";
import { modalReducer } from "./reducers/modalReducer";
import { loginReducer } from "./reducers/loginReducer";
import { languageReducer } from "./reducers/languageReducer";
import { quizReducer } from "./reducers/quizReducer";
import { composeWithDevTools } from "@redux-devtools/extension";

const rootReducer = combineReducers({
    modal: modalReducer,
    login: loginReducer,
    language: languageReducer,
    quiz: quizReducer,
});

export const store = createStore(rootReducer, composeWithDevTools());

export type RootState = ReturnType<typeof rootReducer>;
