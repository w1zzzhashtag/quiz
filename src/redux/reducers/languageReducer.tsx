import { LOCALES } from "../../translations/locales";
import { CHANGE_LANGUAGE } from "../../utils/constants";
interface ILanguageState {
    locale: string;
}

interface ILanguageAction {
    type: typeof CHANGE_LANGUAGE;
    payload: string;
}

const initialState: ILanguageState = {
    locale: LOCALES.RUSSIAN.value,
};

export const languageReducer = (state = initialState, action: ILanguageAction): ILanguageState => {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            return { ...state, locale: action.payload };
        default:
            return state;
    }
};
