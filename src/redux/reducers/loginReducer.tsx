import { authActionTypes } from "../../types/types";

interface ILoginState {
    isLogged: boolean;
}

interface ILoginAction {
    type: string;
}

const initialState: ILoginState = {
    isLogged: false,
};

export const loginReducer = (state = initialState, action: ILoginAction): ILoginState => {
    switch (action.type) {
        case authActionTypes.IS_LOGGED:
            return { ...state, isLogged: true };
        case authActionTypes.IS_UNLOGGED:
            return { ...state, isLogged: false };
        default:
            return state;
    }
};
