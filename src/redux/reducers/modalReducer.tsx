import { POPUP_VISIBILITY } from "../../utils/constants";

interface IModalState {
    isModalOpen: boolean;
}

interface IModalAction {
    type: typeof POPUP_VISIBILITY;
    payload: boolean;
}

const initialState: IModalState = {
    isModalOpen: false,
};

export const modalReducer = (state = initialState, action: IModalAction): IModalState => {
    switch (action.type) {
        case POPUP_VISIBILITY:
            return { ...state, isModalOpen: action.payload };
        default:
            return state;
    }
};
