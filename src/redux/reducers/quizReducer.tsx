import { IQuiz } from "../../types/types";
import { ADD_QUIZ, CHANGE_QUIZZES } from "../../utils/constants";

interface IQuizzesState {
    quizzes: IQuiz[];
}

interface IQuizAction {
    type: typeof ADD_QUIZ | typeof CHANGE_QUIZZES;
    payload: IQuiz;
}

const initialState: IQuizzesState = {
    quizzes: [],
};

export const quizReducer = (state = initialState, action: IQuizAction) => {
    switch (action.type) {
        case ADD_QUIZ:
            return { ...state, quizzes: [...state.quizzes, action.payload] };
        case CHANGE_QUIZZES:
            return {
                ...state,
                quizzes: state.quizzes.map((quiz) => {
                    if (quiz.quizTitle === action.payload.quizTitle) {
                        return { ...quiz, ...action.payload };
                    }
                    return quiz;
                }),
            };
        default:
            return state;
    }
};
