import styled from "styled-components";
import { Layout, Col, Row, Form, List, Modal, Typography, Input, Spin } from "antd";
import { Link } from "react-router-dom";
import { Content } from "antd/es/layout/layout";
import { MinusCircleOutlined } from "@ant-design/icons";
const { Title } = Typography;

interface IFlex {
    justify: string;
    padding?: string;
}

interface IImage {
    width: string;
    minHeight?: string;
}

interface IListItem {
    $active: boolean | null;
}

interface IFormList {
    margin: string;
}

export const Button = styled.button`
    color: ${({ color }) => color};
    background-color: #1677ff;
    box-shadow: 0 2px 0 rgb(5 145 255 / 10%);
    font-size: 14px;
    height: 32px;
    padding: 4px 15px;
    border-radius: 6px;
    border: none;
    &:hover {
        opacity: 0.8;
    }
`;

export const HiddenInput = styled.input`
    opacity: 0;
    height: 0;
    width: 0;
    line-height: 0;
    overflow: hidden;
    padding: 0;
    margin: 0;
`;

export const Flex = styled.div<IFlex>`
    display: flex;
    justify-content: ${(props) => props.justify};
    padding: ${(props) => props.padding};
`;

export const Image = styled.img<IImage>`
    width: ${(props) => props.width};
    min-height: ${(props) => props.minHeight};
`;

export const FileName = styled.p`
    padding: 0;
    margin: 0;
`;

export const FormWrapper = styled.div`
    margin: 0 auto;
    max-width: 600px;
    padding: 50px;
`;

export const BoldText = styled.p`
    font-weight: bold;
`;

export const Text = styled.p`
    color: grey;
    width: 100%;
`;

export const NewLayout = styled(Layout)`
    max-width: 1440px;
    margin: 0 auto;
    min-height: 100vh;
`;

export const RowHeader = styled(Row)`
    color: white;
`;

export const NewLink = styled(Link)`
    color: white;
`;

export const NewCol = styled(Col)`
    text-align: end;
`;

export const NewContent = styled(Content)`
    padding: 20px;
`;

export const DeleteButton = styled(MinusCircleOutlined)`
    position: relative;
    top: 4px;
    margin: 0 8px;
    color: #999;
    font-size: 24px;
    cursor: pointer;
    transition: all 0.3s;
    &:hover {
        color: #777;
    }
    :disabled {
        cursor: not-allowed;
        opacity: 0.5;
    }
`;

export const FormList = styled(List)<IFormList>`
    margin-bottom: ${(props) => props.margin};
`;

export const FormButton = styled(Form.Item)`
    display: flex;
    justify-content: end;
`;

export const ModalCenter = styled(Modal)`
    text-align: center;
`;

export const TitleCenter = styled(Title)`
    text-align: center;
`;

export const RowOptions = styled(Row)`
    margin-bottom: 20px;
`;

export const QuestionInput = styled(Input)`
    width: 90%;
`;

export const IconWrapper = styled.span`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: 20px;
    width: 100px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.7);
    opacity: 0;
    color: #fff;
    font-size: 40px;
    transition: opacity 0.4s linear;
    &:hover {
        opacity: 1;
    }
`;

export const Icon = styled.img`
    width: 25px;
    opacity: 0.5;
    transition: opacity 0.3s linear;
    &:hover {
        opacity: 1;
        cursor: pointer;
    }
`;

export const ListItem = styled(List.Item)<IListItem>`
    cursor: pointer;
    transition: text-decoration-color 0.4s ease-in-out;
    text-decoration: underline;
    text-decoration-color: transparent;
    color: ${(props) => (props.$active ? "#1677ff !important" : "")};
    &:hover {
        text-decoration-color: #1677ff;
    }
`;

export const ListItemText = styled.p`
    padding: 8px 16px 16px;
    opacity: 0.6;
`;

export const Loader = styled(Spin)`
    display: block;
    margin: 30px auto 0;
`;
