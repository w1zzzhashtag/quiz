import ru from "../images/ru.png";
import en from "../images/en.png";

export const LOCALES = {
    ENGLISH: {
        value: "en-US",
        imagesSrc: en,
        label: "English",
    },
    RUSSIAN: {
        value: "ru-RU",
        imagesSrc: ru,
        label: "Русский",
    },
};
