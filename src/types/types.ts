import { Tab } from "rc-tabs/lib/interface";
import { FormInstance } from "antd";

export enum authActionTypes {
    IS_LOGGED = "IS_LOGGED",
    IS_UNLOGGED = "IS_UNLOGGED",
}

export type QuestionsNavigationProps = {
    onNumberChange: (number: string) => void;
    items: Tab[];
    currentQuestion: number;
};

export interface QuestionProps {
    currentQuestion: number;
    imageInfo: IImageInfo[];
    handleInputChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
    listItemActive: number;
    addImage: (obj: IImageInfo) => void;
}

export interface IQuestion {
    type: string;
    question?: string;
    variants?: string[];
}
export interface IQuiz {
    quizTitle: string;
    questions: IQuestion[];
    arr: IImageInfo[];
}
export interface IValues {
    quizTitle: string;
    question?: string;
    variants?: string[];
}
export interface ILoginData {
    password: string;
    email: string;
    remember: boolean;
}

export interface ISelectedImage {
    name: string | null;
}

export interface FileLoadingProps {
    currentQuestion: number;
    imageInfo: IImageInfo[];
    handleInputChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
    listItemActive: number;
    addImage: (obj: IImageInfo) => void;
}

export type UrlImage = string | ArrayBuffer | null;

export type SelectedImage = string | ArrayBuffer | null;

export interface IFormQuizProps {
    form: FormInstance;
    questions: IQuestion[];
    getQuestionsValue: (questions: IQuestion[]) => void;
    changeCurrentQuestion: (currentQuestion: number) => void;
    currentQuestion: number;
    imageInfo: IImageInfo[];
    addImage: (obj: IImageInfo) => void;
    clearImageInfo: () => void;
    listItemActive: number;
}

export interface IQuizzesListProps {
    form: FormInstance;
    getQuestionsValue: (questions: IQuestion[]) => void;
    changeCurrentQuestion: (currentQuestion: number) => void;
    changeListItemActive: (index: number) => void;
    listItemActive: number;
}

export interface IImageInfo {
    url?: UrlImage;
    name?: string;
}
