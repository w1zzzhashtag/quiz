export const CHANGE_LANGUAGE = "CHANGE_LANGUAGE";
export const POPUP_VISIBILITY = "POPUP_VISIBILITY";
export const ADD_QUIZ = "ADD_QUIZ";
export const CHANGE_QUIZZES = "CHANGE_QUIZZES";

export const EMAIL_FOR_LOGIN = "test@yandex.ru";
export const PASSWORD_FOR_LOGIN = "54321";
export const QUESTION_OBJ = {
    type: "Multiple",
    question: "",
    variants: [],
};

export const INITIAL_QUIZ_FORM_VALUES = {
    quizTitle: "",
    questions: [QUESTION_OBJ],
};

export const OPTIONS = {
    wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 24, offset: 0 },
    },
};

export const LIST_WITH_EMPTY_QUIZ = [{ quizTitle: "", questions: [], arr: [] }];
