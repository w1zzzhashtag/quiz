import { IntlShape } from "react-intl";

export const getLanguage = (locale: string) => {
    return import(`../translations/${locale}.ts`);
};

export function getText(intl: IntlShape) {
    return function (id: string) {
        return intl.formatMessage({ id: id, defaultMessage: "message" });
    };
}
